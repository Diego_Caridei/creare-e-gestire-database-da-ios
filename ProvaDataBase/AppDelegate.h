//
//  AppDelegate.h
//  ProvaDataBase
//
//  Created by Diego Caridei on 01/12/12.
//  Copyright (c) 2012 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
