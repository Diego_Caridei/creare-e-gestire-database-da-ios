//
//  ViewController.h
//  ProvaDataBase
//
//  Created by Diego Caridei on 01/12/12.
//  Copyright (c) 2012 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGODatabase.h"

@interface ViewController : UIViewController
- (IBAction)Inserisci:(id)sender;
- (IBAction)Mostra:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *prodotto;

@property (retain, nonatomic) IBOutlet UITextField *quantita;
@end
