//
//  ViewController.m
//  ProvaDataBase
//
//  Created by Diego Caridei on 01/12/12.
//  Copyright (c) 2012 Diego Caridei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize prodotto,quantita;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Inserisci:(id)sender {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    // copy database file to Documents directory
    NSString *targetPath = [[paths objectAtIndex:0]
                            stringByAppendingPathComponent:@"Database.db"];
	
	NSFileManager *fileMgr = [NSFileManager defaultManager];
	if([fileMgr fileExistsAtPath:targetPath]) {
		NSLog(@"File already exists, skipping...");
	}
	else {
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"Database" ofType:@"db"];
        NSLog(@"%@",sourcePath);
        [fileMgr copyItemAtPath:sourcePath toPath:targetPath error:nil];
        NSLog(@"Copied DB named %@ from bundle path to documents directory.", @"Database.db");
    }
    
    
    
    EGODatabase* database = [EGODatabase databaseWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Database.db"]];
    
    //query per inserire  
    EGODatabaseResult* result = [database executeQueryWithParameters:[NSString  stringWithFormat:  @" INSERT INTO prodotti (prodotto,quantità) Values ('%@','%@')", prodotto.text,quantita.text ],nil];
}

- (IBAction)Mostra:(id)sender {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    // copy database file to Documents directory
    NSString *targetPath = [[paths objectAtIndex:0]
                            stringByAppendingPathComponent:@"Database.db"];
	
	NSFileManager *fileMgr = [NSFileManager defaultManager];
	if([fileMgr fileExistsAtPath:targetPath]) {
		NSLog(@"File already exists, skipping...");
	}
	else {
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"Database" ofType:@"db"];
        NSLog(@"%@",sourcePath);
        [fileMgr copyItemAtPath:sourcePath toPath:targetPath error:nil];
        NSLog(@"Copied DB named %@ from bundle path to documents directory.", @"Database.db");
    }
    
    
    
    EGODatabase* database = [EGODatabase databaseWithPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Database.db"]];
    
    //eseguo la query
    EGODatabaseResult* result = [database executeQuery:@"SELECT * FROM prodotti"];
    
    //mostro i risultati 
    for(EGODatabaseRow* row in result) {
        NSLog(@"Nome: %@", [row stringForColumn:@"prodotto"]);
        NSLog(@"Quantità: %@", [row stringForColumn:@"quantità"]);

    }

    
    
}
- (void)dealloc {
   
    [super dealloc];
}
@end
