//
//  main.m
//  ProvaDataBase
//
//  Created by Diego Caridei on 01/12/12.
//  Copyright (c) 2012 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
